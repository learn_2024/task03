﻿namespace Todo.Domains
{
    public class Todo
    {
        public long Id { get; set; }
        public string TodoLabel { get; set; } = default!;
        public bool IsDone { get; set; }
        public DateTime CreatedDate { get; } = DateTime.UtcNow;
        public DateTime? UpdatedDate { get; set; }

    }
}
