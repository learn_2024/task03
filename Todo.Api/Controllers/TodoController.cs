﻿using Microsoft.AspNetCore.Mvc;

namespace Todo.Api.Controllers
{
    public class TodoController : Controller
    {
        private readonly TodoListRepository _todoListRepository;

        public TodoController(TodoListRepository todoListRepository)
        {
            _todoListRepository = todoListRepository;
        }

        [HttpGet]
        [Route("todos")]
        public IActionResult Todos(int limit, int offset)
        {
            var list = _todoListRepository.GetTodoList(limit, offset);
            return Ok(list);
        }

        [HttpGet]
        [Route("todos/{id}")]
        public IActionResult TodoTaskById(long id)
        {
            var item = _todoListRepository.GetTodoTaskById(id);
            if (item != null)
            {
                return Ok(item);
            }
            else
            {
                return NotFound(id);
            }

        }

        [HttpGet]
        [Route("todos/{id}/IsDone")]
        public IActionResult TodosIsDoneById(long id)
        {
            var item = _todoListRepository.GetTodoTaskById(id);
            if (item != null)
            {
                return Ok(new TodoTaskIsDoneResponse() { Id = item.Id, IsDone = item.IsDone });
            }
            else
            {
                return NotFound(id);
            }

        }

        [HttpPost]
        [Route("todos")]
        public IActionResult AddSingleTodo([FromBody] TodoTask todo)
        {
            var item = _todoListRepository.AddTodoTask(todo);
            return Created($"/todos/{item.Id}", item);
        }

        [HttpPut]
        [Route("todos/{id}")]
        public IActionResult UpdateTodo(long id, [FromBody] TodoTask todo)
        {
            var item = _todoListRepository.UpdateTodoTask(id, todo);
            if (item != null)
            {
                return Ok(item);
            }
            else
            {
                return NotFound(id);
            }
        }

        [HttpPatch]
        [Route("/todos/{id}/IsDone")]
        public IActionResult SetIsDoneField(long id, [FromBody] bool isDone)
        {
            var item = _todoListRepository.SetIsDoneField(id, isDone);
            if (item != null)
            {
                return Ok(new TodoTaskIsDoneResponse() { Id = item.Id, IsDone = item.IsDone });
            }
            else
            {
                return NotFound(id);
            }
        }

        [HttpDelete]
        [Route("/todos/{id}")]
        public IActionResult DeleteItem(long id)
        {

            if (_todoListRepository.DeleteTodoTask(id))
            {
                return Ok();
            }
            else
            {
                return NotFound(id);
            }
        }
    }
}
